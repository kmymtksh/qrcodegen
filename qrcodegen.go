package main

import (
  "code.google.com/p/rsc/qr"
  "github.com/codegangsta/cli"
  "io/ioutil"
  "os"
)

func main() {
  app := cli.NewApp()
  app.Name = "qrcodegen"
  app.Usage = "small text from stdin to qrcode.png"
  app.Version = "0.0.1"
  app.Flags = []cli.Flag{
    cli.StringFlag{Name: "out, o", Usage: "output.png (required)"},
    cli.StringFlag{Name: "level, l", Value: "M", Usage: "one of L,M,Q,H"},
  }
  app.Action = func(c *cli.Context) {
    if c.String("out") == "" {
      cli.ShowAppHelp(c)
      os.Exit(1)
    }
    level := qr.M
    switch c.String("level") {
    case "L":
      level = qr.L
    case "M":
      level = qr.M
    case "Q":
      level = qr.Q
    case "H":
      level = qr.H
    }
    bytes, err := ioutil.ReadAll(os.Stdin)
    code, _ := qr.Encode(string(bytes), level)
    o2, err := os.Create(c.String("out"))
    if err != nil {
      panic(err)
    }
    if _, err := o2.Write(code.PNG()); err != nil {
      panic(err)
    }
  }
  app.Run(os.Args)
}
